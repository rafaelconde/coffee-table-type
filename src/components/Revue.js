import React from 'react';

class RevueForm extends React.Component {
  constructor() {
    super();
    this.state = { email: '', error: false };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.changeEmail = this.changeEmail.bind(this);
  }

  changeEmail(event) {
    const { value } = event.target;

    this.setState({
      email: event.target.value,
      error: value ? false : true
    });
  }

  handleSubmit(event) {
    if (!this.state.email.length) {
      this.setState({ error: true }, () => {
        if (this.state.error) {
          event.preventDefault();
        }
      });
    }
  }

  render() {
    return (
      <div id="revue-embed">
        <form onSubmit={this.handleSubmit} action="https://www.getrevue.co/profile/coffeetabletypography/add_subscriber" className="revue-form" method="post" id="revue-form" name="revue-form" target="_blank">
          <div className="revue-form-group">
            <label htmlFor="member_email">Email address</label>
            <input onChange={this.changeEmail} className="revue-form-field" placeholder="Your email address..." type="email" name="member[email]" id="member_email" />
          </div>
          <div className="revue-form-actions">
            <input type="submit" value="Subscribe" name="member[subscribe]" id="member_submit" />
          </div>
        </form>
        {this.state.error &&
          <span aria-live="polite" aria-role="alert" style={{
            fontSize: '.7em',
            color: 'red'
          }}>Please enter an email address</span>
        }
      </div>);
  }
}

export default RevueForm;
