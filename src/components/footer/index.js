import React from 'react';
import './_footer.scss';

const year = new Date().getUTCFullYear();

const Footer = () =>
  (<footer className="footer">
    <div className="container">
      <div className="row">
        <div className="col-sm-12 col-md-7">
          <div className="footer-content">
            <p>Subscriptions are managed by <a href="https://www.getrevue.co">Revue</a>.</p>
            <p>This page was built with Gatsby, hosted on <a href="https://gitlab.com/magalhini/coffee-table-type">GitLab</a>. It uses Freight Sans Pro and FF Tisa Web, both served from Typekit. <a href="https://twitter.com/magalhini">Ricardo Magalhães</a>, {year}.</p>
          </div>
        </div>
      </div>
    </div>
  </footer>);

export default Footer;
