import React from 'react';
import Smoke from '../assets/smoke.svg';
import FoxBody from '../assets/renard-b.png';
import FoxTail from '../assets/renard-t.png';
import FoxNose from '../assets/renard-n.png';

const Renard = () => {
  return (
    <div>
      <div className="fox">
        <span className="fox__author">Illustration by <a href="http://www.genmo.net/">Geneviève Monette</a></span>
        <div className="steam">
          <img className="svg-steam" src={Smoke} />
          <img className="svg-steam" src={Smoke} />
          <img className="svg-steam" src={Smoke} />
        </div>

        <img className="body" src={FoxBody} alt="The quick brown fox drinks a cup of coffee. Illustration done by Geneviève Monette." />
        <img src={FoxNose} alt="Animating nose of the fox" className="nose" />
        <img className="tail" src={FoxTail} alt="Animated tail of the fox." />
      </div>
    </div>
  );
};

export default Renard;
